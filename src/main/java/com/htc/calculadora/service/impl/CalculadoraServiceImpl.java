package com.htc.calculadora.service.impl;

import org.springframework.stereotype.Component;

import com.htc.calculadora.service.CalculadoraService;

@Component
public class CalculadoraServiceImpl implements CalculadoraService {

	@Override
	public double suma(double n1, double n2) {
		
		return n1 + n2;
	}

	@Override
	public double resta(double n1, double n2) {
		
		return n1 - n2;
	}

	@Override
	public double multiplicacion(double n1, double n2) {
		
		return n1 * n2;
	}

	@Override
	public double division(double n1, double n2) {

		if (n2 == 0)
			throw new IllegalArgumentException("NO SE PUEDE DIVIDIR UN CERO");

		return n1 / n2;

	}

}

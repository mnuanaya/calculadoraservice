package com.htc.calculadora.ws.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.calculadora.operaciones.Error;
import com.htc.calculadora.operaciones.GetDivisionRequest;
import com.htc.calculadora.operaciones.GetDivisionResponse;
import com.htc.calculadora.operaciones.GetMultiplyRequest;
import com.htc.calculadora.operaciones.GetMultiplyResponse;
import com.htc.calculadora.operaciones.GetRestaRequest;
import com.htc.calculadora.operaciones.GetRestaResponse;
import com.htc.calculadora.operaciones.GetSumaRequest;
import com.htc.calculadora.operaciones.GetSumaResponse;
import com.htc.calculadora.service.CalculadoraService;

@Endpoint
public class CalculadoraEndpoint {

	private static final String NAMESPACE_URI = "http://operaciones.calculadora.htc.com";

	private CalculadoraService calculadoraService;

	@Autowired
	public CalculadoraEndpoint(CalculadoraService calculadoraService) {
		super();
		this.calculadoraService = calculadoraService;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getSumaRequest")
	@ResponsePayload
	public GetSumaResponse getState(@RequestPayload GetSumaRequest request) {
		GetSumaResponse response = new GetSumaResponse();
		response.setResultado(calculadoraService.suma(request.getNumer1(), request.getNumero2()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getRestaRequest")
	@ResponsePayload
	public GetRestaResponse getState(@RequestPayload GetRestaRequest request) {
		GetRestaResponse response = new GetRestaResponse();
		response.setResultadoResta(calculadoraService.resta(request.getNumero1(), request.getNumero2()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMultiplyRequest")
	@ResponsePayload
	public GetMultiplyResponse getState(@RequestPayload GetMultiplyRequest request) {
		GetMultiplyResponse response = new GetMultiplyResponse();
		response.setRespuestaMultiply(calculadoraService.multiplicacion(request.getNumero1(), request.getNumero2()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getDivisionRequest")
	@ResponsePayload
	public GetDivisionResponse getState(@RequestPayload GetDivisionRequest request) {
		GetDivisionResponse response = new GetDivisionResponse();
		try {
			response.setResultadoDivision(calculadoraService.division(request.getNumero1(), request.getNumero2()));

		} catch (IllegalArgumentException e) {
			Error error = new Error();
			error.setId(30023);
			error.setMessage(e.getMessage());

			response.setErrorDivision(error);

			return response;

		}
		return response;
	}

}

package com.htc.calculadora.exception;

public class DivitionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private static final long DEFAULT_CODE = 300023;
	private long code;
	private final String description;

	public DivitionException(Exception e) {
		super(e);
		this.code = DEFAULT_CODE;
		this.description = e.getMessage();
	}

	public DivitionException(String description, Exception ex) {
		super(ex);
		this.code = DEFAULT_CODE;
		this.description = description;

	}

	public DivitionException(long code, String description, Exception e) {
		super(e);
		this.code = code;
		this.description = description;
	}

	public DivitionException(long code, String description) {
		this.code = code;
		this.description = description;
	}

	public DivitionException(String description) {
		// this.code = Long.valueOf( env.getProperty("errorDivision"));
		this.code = DEFAULT_CODE;
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("DivitionException code: ").append(code).append("Description:").append(description);
		return sb.toString();
	}

}

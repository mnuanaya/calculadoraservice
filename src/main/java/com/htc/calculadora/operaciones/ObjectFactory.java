//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.24 a las 03:52:43 PM CST 
//


package com.htc.calculadora.operaciones;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.htc.calculadora.operaciones package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.htc.calculadora.operaciones
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetMultiplyRequest }
     * 
     */
    public GetMultiplyRequest createGetMultiplyRequest() {
        return new GetMultiplyRequest();
    }

    /**
     * Create an instance of {@link GetDivisionResponse }
     * 
     */
    public GetDivisionResponse createGetDivisionResponse() {
        return new GetDivisionResponse();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link GetSumaRequest }
     * 
     */
    public GetSumaRequest createGetSumaRequest() {
        return new GetSumaRequest();
    }

    /**
     * Create an instance of {@link GetSumaResponse }
     * 
     */
    public GetSumaResponse createGetSumaResponse() {
        return new GetSumaResponse();
    }

    /**
     * Create an instance of {@link GetMultiplyResponse }
     * 
     */
    public GetMultiplyResponse createGetMultiplyResponse() {
        return new GetMultiplyResponse();
    }

    /**
     * Create an instance of {@link GetRestaResponse }
     * 
     */
    public GetRestaResponse createGetRestaResponse() {
        return new GetRestaResponse();
    }

    /**
     * Create an instance of {@link GetRestaRequest }
     * 
     */
    public GetRestaRequest createGetRestaRequest() {
        return new GetRestaRequest();
    }

    /**
     * Create an instance of {@link GetDivisionRequest }
     * 
     */
    public GetDivisionRequest createGetDivisionRequest() {
        return new GetDivisionRequest();
    }

}
